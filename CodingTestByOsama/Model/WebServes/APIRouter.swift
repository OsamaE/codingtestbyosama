//
//  APIRouter.swift
//  Gift
//
//  Created by osama on 4/22/20.
//  Copyright © 2020 Gift. All rights reserved.
//

import Foundation
import Alamofire
enum APIRouter: URLRequestConvertible {
      
    // MARK: - Registrtions
    case realestates
    

    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .realestates:
            return .get
        
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .realestates:
            return "realestates"
       
        }
    }
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
            
        case .realestates:
            return nil
           
        }
        
    }
    // MARK: - headers
    private  var headers: [String:String]? {
        switch self {
            
        case .realestates:
            return nil
       
        }
        
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constant.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
       
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        urlRequest.setValue("Accept-Language", forHTTPHeaderField: "en")
        if let heder = headers {
            if heder.isEmpty {
                print("Notoken")
            }else{
//                loadUser { (error, user) in
//                    if user?.accsesToken != nil {
//
//                        let token = user?.accsesToken
//                        let fcmtoken = user?.fcmToken
//                        urlRequest.setValue("Bearer \(token ?? "")", forHTTPHeaderField:"Authorization")
//                        urlRequest.setValue("\(fcmtoken ?? "")", forHTTPHeaderField: "fcmToken")
//                        print(token,"headers")
//                        print(user?.fcmToken , "headers")
//                    }else {
//                        let fcmtoken = user?.fcmToken
//
//                        urlRequest.setValue("\(fcmtoken ?? "")", forHTTPHeaderField: "fcmToken")
//
//                    }
                    
                    
                
            }
            
        }
        
        if let parameters = parameters {
            do {
                
                let par = try JSONSerialization.data(withJSONObject: parameters, options: [])
                
                urlRequest.httpBody = par
                print(parameters)
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        return urlRequest
        
    }
    
}

