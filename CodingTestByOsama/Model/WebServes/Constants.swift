//
//  Constants.swift
//  Gift
//
//  Created by osama on 4/22/20.
//  Copyright © 2020 Gift. All rights reserved.
//

import Foundation
import Alamofire
struct Constant {
    struct ProductionServer {
        static let baseURL = "http://private-91146-mobiletask.apiary-mock.com/"
    }
    //MARK: - API Parametr Key
    struct APIParameterKey {
        
        // Register: -
        static let name = "name"
        static let email = "email"
        static let UserPasswored = "user_pass"
        static let mobile = "mobile"
        static let password = "password"
        static let fcm_token = "fcm_token"
        static let device_type = "device_type"
        static let city_id = "city_id"
        static let file_name = "file_name"
        static let birthdate = "birthdate"
        
        
    }
    
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
    case AcceptLanguage = "Accept-Language"
}
struct WebserviceConfig {
    /// Generates common headers specific to APIs. Can also accept additional headers if demanded by specific APIs.
    ///
    /// - Returns: A configured header in the form of JSON dictionary.
    func generateHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"
        
        return headerDict
    }
}
