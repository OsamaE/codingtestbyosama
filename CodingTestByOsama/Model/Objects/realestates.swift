//
//  realestates.swift
//  CodingTestByOsama
//
//  Created by osama on 10/16/20.
//  Copyright © 2020 osama. All rights reserved.
//

import Foundation

struct realestates : Codable {
    let items : [Items]?

    enum CodingKeys: String, CodingKey {

        case items = "items"
    }
    
    
    struct Items : Codable {
        let id : Int?
        let title : String?
        let price : Int?
        let location : Location?
        let images : [Images]?

        enum CodingKeys: String, CodingKey {

            case id = "id"
            case title = "title"
            case price = "price"
            case location = "location"
            case images = "images"
        }

        
    }
    
    
    struct Images : Codable {
        let id : Int?
        let url : String?

        enum CodingKeys: String, CodingKey {

            case id = "id"
            case url = "url"
        }

       
    }
    
    
    struct Location : Codable {
        let address : String?
        let latitude : Double?
        let longitude : Double?

        enum CodingKeys: String, CodingKey {

            case address = "address"
            case latitude = "latitude"
            case longitude = "longitude"
        }

       
    }


}
