//
//  HomeVC.swift
//  CodingTestByOsama
//
//  Created by osama on 10/16/20.
//  Copyright © 2020 osama. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    var Data:realestates?
    @IBOutlet weak var HomeTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.HomeTableView.estimatedRowHeight = 1
        self.HomeTableView.rowHeight = UITableView.automaticDimension

    }

    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
    }
    
   
    
}

extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Data?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        // I use default uitableviewcell you can custom it :)
        UITableViewCell {
        let cell: UITableViewCell = {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
            }
            return cell
        }()
            
            let object = self.Data?.items?[indexPath.row]
            cell.textLabel?.text = "\(object?.title ?? "")"
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DetailsVC") as! DetailsVC
        let object = self.Data?.items?[indexPath.row]
        vc.Data = object
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



extension HomeVC{
    
       func loadData() {
           NetworkClient.Request(self,realestates.self, router: APIRouter.realestates) { (sucsses, error) in
               if error == nil {
                   self.Data = sucsses
                   self.HomeTableView.reloadData()
               }
           }
       }
}
